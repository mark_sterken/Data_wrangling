

###Load & install packages
    install.packages("colorspace")
    install.packages("ggplot2")
    install.packages("dplyr")
    install.packages("tidyr")
    install.packages("RColorBrewer")
    install.packages("gridExtra")
    install.packages("VennDiagram")


###load
    library("colorspace")
    library("ggplot2") ###http://ggplot2.org/
    library("dplyr")
    library("tidyr")
    library("RColorBrewer")
    library(gridExtra)
    library("VennDiagram")


###Set plotting theme
    presentation <- theme(axis.text.x = element_text(size=16, face="bold", color="black"),
                          axis.text.y = element_text(size=16, face="bold", color="black"),
                          axis.title.x = element_text(size=20, face="bold", color="black"),
                          axis.title.y = element_text(size=20, face="bold", color="black"),
                          strip.text.x = element_text(size=20, face="bold", color="black"),
                          strip.text.y = element_text(size=20, face="bold", color="black"),
                          plot.title = element_text(size=24, face="bold"),
                          panel.grid.minor = element_blank(),
                          panel.grid.major = element_blank(),
                          legend.position = "right")

    blank_theme <- theme(plot.background = element_blank(),
                         panel.grid.major = element_blank(),
                         panel.grid.minor = element_blank(),
                         panel.border = element_blank(),
                         panel.background = element_blank(),
                         axis.title.x = element_blank(),
                         axis.title.y = element_blank(),
                         axis.text.x = element_blank(),
                         axis.text.y = element_blank(),
                         axis.ticks = element_blank())

